-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 05, 2017 at 04:14 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Assignment`
--

-- --------------------------------------------------------

--
-- Table structure for table `Clubs`
--

CREATE TABLE `Clubs` (
  `id` varchar(50) NOT NULL,
  `clubName` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `county` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Entry`
--

CREATE TABLE `Entry` (
  `userName` varchar(50) DEFAULT NULL,
  `fallYear` int(4) NOT NULL,
  `sdate` date DEFAULT NULL,
  `area` varchar(50) DEFAULT NULL,
  `distance` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Season`
--

CREATE TABLE `Season` (
  `fallYear` int(4) NOT NULL,
  `clubName` varchar(50) DEFAULT NULL,
  `id` varchar(50) DEFAULT NULL,
  `userName` varchar(50) DEFAULT NULL,
  `distance` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Skiers`
--

CREATE TABLE `Skiers` (
  `userName` varchar(25) NOT NULL,
  `firstName` varchar(60) DEFAULT NULL,
  `lastName` varchar(250) DEFAULT NULL,
  `yearOfBirth` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Clubs`
--
ALTER TABLE `Clubs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Entry`
--
ALTER TABLE `Entry`
  ADD KEY `userName` (`userName`),
  ADD KEY `fallYear` (`fallYear`);

--
-- Indexes for table `Season`
--
ALTER TABLE `Season`
  ADD PRIMARY KEY (`fallYear`),
  ADD KEY `id` (`id`),
  ADD KEY `userName` (`userName`);

--
-- Indexes for table `Skiers`
--
ALTER TABLE `Skiers`
  ADD PRIMARY KEY (`userName`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Entry`
--
ALTER TABLE `Entry`
  ADD CONSTRAINT `Entry_ibfk_1` FOREIGN KEY (`userName`) REFERENCES `Skiers` (`userName`),
  ADD CONSTRAINT `Entry_ibfk_2` FOREIGN KEY (`fallYear`) REFERENCES `Season` (`fallYear`);

--
-- Constraints for table `Season`
--
ALTER TABLE `Season`
  ADD CONSTRAINT `Season_ibfk_1` FOREIGN KEY (`id`) REFERENCES `Clubs` (`id`),
  ADD CONSTRAINT `Season_ibfk_2` FOREIGN KEY (`userName`) REFERENCES `Skiers` (`userName`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

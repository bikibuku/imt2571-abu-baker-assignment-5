<?php
class Season{
	private $fallYear = null;
	private $clubName = null;
	private $userName = null;
	
	
	public function __construct($doc, $dbf)
	{  
		if ($dbf){
			$this->fallYear = $doc->getAttribute("fallYear");
			$searchNode = $doc->getElementsByTagName("Skiers");
			foreach($searchNode as $searchNodes)						// INSIDE FALLYEAR
			{
				$this->clubName = $searchNodes->getAttribute("clubId");			// GET THE ID
				$searchNode2 = $searchNodes->getElementsByTagName("Skier");		// INSIDE SKIER
				foreach($searchNode2 as $searchNodes2)
				{
					$totalDistance = 0;	
					$this->userName = $searchNodes2->getAttribute("userName");	// GET USERNAME
					$searchNode3 = $searchNodes2->getElementsByTagName("Entry");	// ENTRY
					foreach($searchNode3 as $searchNodes3)				// TO GET DISTANCE AND OTHER DATA
					{					
						 $distance = $searchNodes3->getElementsByTagName("Distance");
						 $totalDistance += $distance;				// ADDS UP TOTALDISTANCE
						 $area = $searchNodes3->getElementsByTagName("Area");	// GET AREA
						 $date = $searchNodes3->getElementsByTagName("Date");	// GET DATE AND INSERT
						 $opp = $dbf->prepare('INSERT INTO Logs(userName, fallYear, sdate, area, distance)'
				 			. ' VALUES(:userName, :fallYear, :sdate, :area, :distance)'
							);
						 $opp->bindValue(':userName', $this->userName);		// BINDING 
						 $opp->bindValue(':fallYear', $this->fallYear);
						 $opp->bindValue(':sdate', $this->sdate);
				 		 $opp->bindValue(':area', $this->area);
						 $opp->bindValue(':distance', $this->distance);
						 $opp->execute();
					}
					
				}			
					      $opp = $dbf->prepare('INSERT INTO Season(fallYear, clubName, userName, Distance)'			
			        			. ' VALUES(:fallYear, :clubName, :userName, :Distance)'
							);
					      $opp->bindValue(':fallYear', $this->fallYear);
					      $opp->bindValue(':clubName', $this->clubName);
					      $opp->bindValue(':userName', $this->userName);
					      $opp->bindValue(':Distance', $this->Distance);				
				}				
		}else{
		echo "dbf failed!";		
		}
	}
     }





?>

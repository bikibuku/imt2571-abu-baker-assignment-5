<?php

	include_once("Clubs.php");
	include_once("db.php");
	include_once("Skiers.php");
	include_once("Season.php");

	class XMLModel{
	public $db = null;
	public $doc = null;
	public $dbf = null;
	
	public function __construct(){
	$this->doc = new DOMDocument;	
	$this->doc->load("SkierLogs.xml");	
	$this->xpath = new DOMXPath($this->doc);
	$this->dbf = new dbfor($this->db);
	$this->readClubs();
	$this->readSkiers();
	$this->readSeason();
	
	
	}

	public function readClubs()
	{
		if($this->doc)
		{
			$ClubsNodes = $this->doc->getElementsByTagName("Club");
			for($a = 0; $a < $ClubsNodes->length; $a++)
			{
				$club = new Club($ClubsNodes[$a], $this->db);
			}		
		}
	}

	public function readSkiers()
	{
		if($this->doc)
		{
			$query = '//SkierLogs/Skiers/Skier';
			$skierNodes = $this->xpath->query($query);			
			foreach ($skierNodes as $skier)
			{
				$skier = new Skier($skier, $this->db);
			}
		}
	}
	
	public function readSeason()
	{
		if($this->doc)
		{
			$query = '//SkierLogs/Season';
			$seasonNodes = $this->xpath->query($query);
			foreach($seasonNodes as $sesongNode) 
			{
				$sesong = new Season($sesongNode, $this->db);
			}
		}
	}



}	
?>

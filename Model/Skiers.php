<?php
class Skier{
	private $userName = null;
	private $firstName = null;
	private $lastName = null;
	private $yearOfBirth = null;

	
	
	public function __construct($doc, $dbf)
	{  
		if ($dbf){
        		$this->userName = $doc->getAttribute("userName");				
       	 		$this->firstName = $doc->getElementsByTagName("FirstName")->item(0)->nodeValue;	
			$this->lastName = $doc->getElementsByTagName("LastName")->item(0)->nodeValue;			
			$this->yearOfBirth = $doc->getElementsByTagName("YearOfBirth")->item(0)->nodeValue;
	
			$opp = $dbf->prepare('INSERT INTO Skiers(userName, firstName, lastName, yearOfBirth)'
				 . ' VALUES(:userName, :firstName, :lastName, :yearOfBirth)'
				);
			$opp->bindValue(':userName', $this->userName);
			$opp->bindValue(':firstName', $this->firstName);
			$opp->bindValue(':lastName', $this->lastName);
			$opp->bindValue(':yearOfBirth', $this->yearOfBirth);
			$opp->execute();
    		
		}else{
		echo "dbf failed!";		
		}
	}
     }





?>

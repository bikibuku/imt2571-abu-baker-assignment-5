<?php
class Club{
	private $id;
	private $clubName;
	private $city;
	private $county;

	
	
	public function __construct($doc, $dbf)
	{  
		if ($dbf){
        		$this->id = $doc->getAttribute("id");				
       	 		$this->clubName = $doc->getElementsByTagName("Name")->item(0)->nodeValue;	
			$this->city = $doc->getElementsByTagName("City")->item(0)->nodeValue;			
			$this->county = $doc->getElementsByTagName("County")->item(0)->nodeValue;
	
			$opp = $dbf->prepare('INSERT INTO Clubs(id,clubName,city, county)'
				 . ' VALUES(:id, :clubName, :city, :county)'
				);
			$opp->bindValue(':id', $this->id);
			$opp->bindValue(':clubName', $this->clubName);
			$opp->bindValue(':city', $this->city);
			$opp->bindValue(':county', $this->county);
			$opp->execute();
    		
		}else{
		echo "dbf failed!";		
		}
	}
     }
?>
